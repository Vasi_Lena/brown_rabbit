window.onload = function(){
    document.body.innerHTML = document.body.innerHTML + "<div>  </div>";
};
(function() {
    var interval = 3000,
        duration = 500,
        slides = document.getElementById('css-slider').querySelectorAll('li');

    var rotator = {
        state: false,
        start: function() {
            this.state = setInterval(function() {
                changeSlide('next');
            }, interval);
            console.log('rotator started');
            document.getElementById('pause-play').setAttribute('data-state', 'play');
        }, stop: function() {
            clearInterval(this.state);
            console.log('rotator stopped');
            this.state = false;
            document.getElementById('pause-play').setAttribute('data-state', 'pause');
        }, pauseResume: function(call) {
            if (this.state == false) this.start()
            else this.stop()
            if (typeof call === 'function') call();
        }
    }
    rotator.start();

    for (var i = 0; i < slides.length; i++)
        slides[i].style.transitionDuration = (duration / 1000)+'s';

    var changeSlide = function(direction) {
        var slider = document.getElementById('css-slider'),
            noSlides = slider.children.length,
            topSlide = slider.children[noSlides - 1],
            toMove = (direction == 'next' ? topSlide : slider.firstElementChild);

        if (event && rotator.state != false) rotator.stop();

        if (direction == 'prev')
            slider.insertBefore(slider.firstElementChild, topSlide);

        topSlide.style.opacity = '0';
        setTimeout(function() {
            if (direction == 'next') slider.insertBefore(topSlide, slider.firstElementChild)
            if (direction == 'prev') slider.insertBefore(topSlide, topSlide.previousElementSibling)
            topSlide.style.opacity = '1';
        }, duration);
    }

    document.addEventListener('keydown', function(event) {
        console.log(event.code+" pressed");
        if (event.keyCode == 39) changeSlide('next')
        if (event.keyCode == 37) changeSlide('prev')
        if (event.keyCode == 32) rotator.pauseResume()
    });

    document.getElementById('css-slider').addEventListener('mouseenter', function(event) {
        console.log('mouseenter');
        if (rotator.state != false) rotator.stop();
    });
    document.getElementById('css-slider').addEventListener('mouseout', function(event) {
        console.log('mouseout');
        if (!rotator.state) rotator.start();
    });
    document.getElementById('prev').addEventListener('click', function(event) {
        changeSlide('prev');
    });
    document.getElementById('pause-play').addEventListener('click', function(event) {
        var pausePlay = this;
        rotator.pauseResume();
    });
    document.getElementById('next').addEventListener('click', function(event) {
        changeSlide('next');
    });
})();
